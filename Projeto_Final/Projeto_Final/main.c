#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

struct aluno{
    int num_aluno;
    char curso[50];
    int ano_matricula;
    char regime[50];
    struct exame *exame[30];
    int numero_exames;
};
typedef struct lista_alunos* Lista;

struct disciplina{
    char nome[50];
    char docente[50];
};

struct data{
    int dia;
    int mes;
    int ano;
    int hora;
    int minuto;
};

struct exame{
    struct disciplina *disciplina_exame;
    char nome_disciplina[50];
    char epoca[30];
    struct data data_exame;
    int duracao;
    int num_salas;
    char *sala[30][4];
    int num_alunos_inscr;
    int alunos_inscritos[300];

};

struct lista_alunos{
    struct aluno *dados;
    struct lista_alunos *prox;
};

struct lista_disciplinas{
    struct disciplina *dados_D;
    struct lista_disciplinas *prox_D;
};

struct lista_exames{
    struct exame *dados_E;
    struct lista_exames *prox_E;
};

typedef struct lista_alunos_inscr *Lista_I;
typedef struct lista_exames *Lista_E;
typedef struct lista_exames Elem_E;
typedef struct lista_disciplinas *Lista_D;
typedef struct lista_disciplinas Elem_D;
typedef struct lista_alunos Elem;


void RetiraEnter(char *str);
void print_list(Lista* li);
int menu(void);
int opcao(int opt,Lista* li,Lista_D* li_D,Lista_E *li_E);
int lista_vazia(Lista* li);
int lista_vazia_D(Lista_D* li_D);
int lista_vazia_E(Lista_E* li_E);
void liberta_lista(Lista* li);
void liberta_lista_D(Lista_D* li_D);
void liberta_lista_E(Lista_E* li_E);
int ins_lis_ord_alunos(Lista* li,struct aluno al);
int consulta_lista_num(Lista *li,int num_aluno);
int consulta_lista_Apaga(Lista *li,int num_aluno);
int ins_lis_ord_disci(Lista_D* li_D,struct disciplina dis);
int consulta_lista_Apaga_dis(Lista_D *li_D,char *nome_disciplina);
int consulta_lista_dis(Lista_D *li_D,char *nome_disciplina);
int StringsIguais(char s1[], char s2[]);
int consulta_lista_Endereco(Lista_D *li_D,char *nome_disciplina,struct exame *endereco);
int conta_exam(Lista_E* li_E);
int ins_lis_ord_exames(Lista_E *li_E, Lista_D *li_D);
void print_list_E(Lista_E* li_E);
int StringsIguais_E(char *s1, char *s2);
int consulta_lista_Apaga_Exam(Lista_E *li_E,Lista_D *li_D);
int inscrever_alunos(Lista *li,Lista_E *li_E,Lista_D *li_D);
int configura_salas(Lista_E *li_E);
int verifica_data(struct data data1,struct data data2);
int desinscrever_alunos(Lista *li,Lista_D *li_D,Lista_E *li_E);
int listar_alunos(Lista *li);
int verifica_aluno(Lista *li,int num_aluno);
int verifica_salas(Lista_E *li_E);
int escrever_ficheiro(Lista *li);
int conta_exam_aluno(Lista* li);
int ler_ficheiros(Lista* li);
int escrever_ficheiro_disc(Lista_D *li_D);
int ler_ficheiros_disc(Lista_D* li_D);
int escrever_ficheiro_exame(Lista_E *li_E);
int ler_ficheiros_exame(Lista_E *li_E);
int conta_disc(Lista_D* li_D);
int ins_lis_ord_exames_fich(Lista_E *li_E,struct exame exam);
int consulta_novo_endereco(Lista_D *li_D,Lista_E *li_E);
int consulta_novo_endereco_aluno(Lista_E *li_E,Lista *li);
int converter(char *s1);
void print_list_aluno(Lista* li);




Lista_E* create_list_E(){
    Lista_E* li= (Lista_E*) malloc(sizeof(Lista_E));
    if(li!=NULL)
        *li=NULL;
    return li;
};

Lista_D* create_list_D(){
    Lista_D* li= (Lista_D*) malloc(sizeof(Lista_D));
    if(li!=NULL)
        *li=NULL;
    return li;
};


Lista* create_list(){
    Lista* li= (Lista*) malloc(sizeof(Lista));
    if(li!=NULL)
        *li=NULL;
    return li;
};
void print_list_D(Lista_D* li_D)
{
    Elem_D *no = *li_D;
    while(no != NULL)
    {
        printf("Disciplina: ");
        printf("%s\n",no->dados_D->nome);
        printf("Docente: ");
        printf("%s\n",no->dados_D->docente);
        no = no->prox_D;
        printf("\n");

    }
}

int main()
{
    setlocale(LC_ALL,"Portuguese");
    int opt;
    Lista *li;
    Lista_D *li_D;
    Lista_E * li_E;
    li=create_list();
    li_D=create_list_D();
    li_E=create_list_E();
    ler_ficheiros_disc(li_D);
    ler_ficheiros_exame(li_E);
    consulta_novo_endereco(li_D,li_E);
    ler_ficheiros(li);
    consulta_novo_endereco_aluno(li_E,li);
    do{
		opt=menu();
        opt = opcao(opt,li,li_D,li_E);
	}while(opt>=0);

    return 0;

}

int menu(void){

    printf("-----------------------MENU-----------------------\n");
    int opt;
    char *test = malloc(50*sizeof(char));
    printf("1-Criar dados dos alunos.\n");
    printf("2-Alterar dados dos alunos.\n");
    printf("3-Apagar dados dos alunos.\n");
    printf("4-Criar dados das disciplinas.\n");
    printf("5-Alterar dados das disciplinas.\n");
    printf("6-Apagar dados das disciplinas.\n");
    printf("7-Criar exames.\n");
    printf("8-Configurar salas.\n");
    printf("9-Apagar exames.\n");
    printf("10-Listar exames.\n");
    printf("11-Inscrever alunos.\n");
    printf("12-Desinscrever alunos.\n");
    printf("13-Listar alunos inscritos num exame.\n");
    printf("14-Listas exames em que um aluno esta inscrito.\n");
    printf("15-Verficar salas reservadas para um exame.\n");
    printf("16-Listar disciplinas.\n");
    printf("17-Listar alunos.\n");
    printf("18-Sair.\n");
    printf("Op��o escolhida: ");
    scanf("%s",test);
    opt = atoi(test);
    return opt;
    }

int opcao(int opt,Lista* li,Lista_D* li_D,Lista_E* li_E){
    setlocale(LC_ALL,"Portuguese");
    struct aluno alu;
    struct data data_aux;
    int *numero_aluno;
    struct aluno auxiliar;
    struct disciplina dis;
    char nome_disciplina[50];
	switch(opt){
		case 0:
		    system("CLS");
            printf("Comando inv�lido!\n");
			break;
		case 1:
            printf("Numero de aluno:\n");
            scanf("%d",&alu.num_aluno);
            if(verifica_aluno(li,alu.num_aluno)==1)
                break;
            printf("Curso:\n");
            fflush(stdin);
            fgets(alu.curso,50,stdin);
            RetiraEnter(alu.curso);
            converter(alu.curso);
            printf("Ano de matricula:\n");
            scanf("%d",&alu.ano_matricula);
            printf("Regime:\n(normal, trabalhador-estudante, atleta, dirigente-associativo, erasmus)\n");
            fflush(stdin);
            fgets(alu.regime,50,stdin);
            RetiraEnter(alu.regime);
            converter(alu.regime);
            alu.numero_exames=0;
            ins_lis_ord_alunos(li,alu);
			break;
		case 2:
		    printf("Introduza o n�mero do aluno:\n");
		    scanf("%d",&numero_aluno);
		    consulta_lista_num(li,numero_aluno);
			break;
		case 3:
		    printf("Introduza o n�mero do aluno:\n");
		    scanf("%d",&numero_aluno);
		    consulta_lista_Apaga(li,numero_aluno);
			break;
		case 4:
            printf("Nome:\n");
            fflush(stdin);
            fgets(dis.nome,50,stdin);
            RetiraEnter(dis.nome);
            converter(dis.nome);
            printf("Docente:\n");
            fflush(stdin);
            fgets(dis.docente,50,stdin);
            RetiraEnter(dis.docente);
            converter(dis.docente);
            ins_lis_ord_disci(li_D,dis);
			break;
		case 5:
		    printf("Introduza o nome disciplina:");
		    fflush(stdin);
            fgets(nome_disciplina,50,stdin);
            RetiraEnter(nome_disciplina);
            converter(nome_disciplina);
            consulta_lista_dis(li_D,nome_disciplina);
			break;
		case 6:
            printf("Introduza o nome da disciplina:\n");
		    fflush(stdin);
            fgets(nome_disciplina,50,stdin);
            RetiraEnter(nome_disciplina);
            converter(nome_disciplina);
		    consulta_lista_Apaga_dis(li_D,nome_disciplina);
			break;
		case 7:
            ins_lis_ord_exames(li_E,li_D);
			break;
		case 8:
		    configura_salas(li_E);
		    break;
        case 9:
            consulta_lista_Apaga_Exam(li_E,li_D);
            break;
        case 10:
            print_list_E(li_E);
            break;
        case 11:
            inscrever_alunos(li,li_E,li_D);
            break;
        case 12:
            desinscrever_alunos(li,li_D,li_E);
            break;
        case 13:
            listar_alunos(li);
            break;
        case 14:
            print_list(li);
            break;
        case 15:
            verifica_salas(li_E);
            break;
        case 16:
            print_list_D(li_D);
            break;
        case 17:
            print_list_aluno(li);
            break;
        case 18:
            escrever_ficheiro(li);
            escrever_ficheiro_disc(li_D);
            escrever_ficheiro_exame(li_E);
            liberta_lista(li);
            liberta_lista_D(li_D);
            liberta_lista_E(li_E);
            exit(0);
            break;
		default:
		    system("CLS");
			printf("Comando inv�lido!\n");
	}
}
int lista_vazia_E(Lista_E* li_E){
    if(li_E==NULL){
        return 1;
    }
    if(*li_E == NULL){
        return 1;
    }
    return 0;
}
int lista_vazia_D(Lista_D* li_D){
    if(li_D==NULL){
        return 1;
    }
    if(*li_D == NULL){
        return 1;
    }
    return 0;
}

int consulta_lista_Apaga_dis(Lista_D *li_D,char *nome_disciplina)
{
    if(li_D==NULL)
        return 0;
    Elem_D *ant,*no=*li_D;
    while(no != NULL &&(StringsIguais(no->dados_D->nome,nome_disciplina))){
        ant=no;
        no = no->prox_D;
    }
    if(no==NULL)
        return 0;
    if(no==*li_D)
        *li_D = no->prox_D;
    else
        ant->prox_D = no->prox_D;
    free(no);
    return 1;
}
int lista_vazia(Lista* li){

    if(li==NULL){
        return 1;
    }
    if(*li == NULL){
        return 1;
    }
    return 0;
}
void liberta_lista(Lista* li){
    if(li!= NULL){
        Elem* no;
        while((*li) != NULL){
            no = *li;
            *li = (*li)->prox;
            free(no);
        }
        free(li);
    }
}
void liberta_lista_D(Lista_D* li_D){
    if(li_D!= NULL){
        Elem_D* no;
        while((*li_D) != NULL){
            no = *li_D;
            *li_D = (*li_D)->prox_D;
            free(no);
        }
        free(li_D);
    }
}
void liberta_lista_E(Lista_E* li_E){
    if(li_E!= NULL){
        Elem_E* no;
        while((*li_E) != NULL){
            no = *li_E;
            *li_E = (*li_E)->prox_E;
            free(no);
        }
        free(li_E);
    }
}
void print_list(Lista* li)
{
    int num_aluno;
    printf("Introduza o numero de aluno:\n");
    scanf("%d",&num_aluno);
    int i;
    Elem *no = *li;
    while(no != NULL)
    {
        if((no->dados->num_aluno==num_aluno)){
        printf("Curso: ");
        printf("%s\n",no->dados->curso);
        printf("Ano de matricula: ");
        printf("%d\n",no->dados->ano_matricula);
        printf("Regime: ");
        printf("%s\n",no->dados->regime);
        printf("Exames em que esta inscrito: ");
        for (i=0;i<no->dados->numero_exames;i++){
                if(no->dados->exame[i]!=NULL)
                    printf("%s, ",no->dados->exame[i]->disciplina_exame->nome);
        }
        printf("\n");}
        no = no->prox;

    }
}

void print_list_aluno(Lista* li)
{
    Elem *no = *li;
    while(no != NULL)
    {
        printf("Numero de aluno: ");
        printf("%d\n",no->dados->num_aluno);
        printf("Curso: ");
        printf("%s\n",no->dados->curso);
        printf("Ano de matricula: ");
        printf("%d\n",no->dados->ano_matricula);
        printf("Regime: ");
        printf("%s\n",no->dados->regime);
        no = no->prox;
        printf("\n");
    }
}

/*Fun�oes alunos*/
int ins_lis_ord_alunos(Lista* li,struct aluno al){

    if(li == NULL){
        return 0;
    }

    Elem *no =(Elem*)malloc(sizeof(Elem));
    if(no==NULL){
        return 0;
    }

    (no->dados)=malloc(sizeof(struct aluno));
    *(no->dados) = al;

    if(lista_vazia(li)){
        no->prox = (*li);
        *li = no;
        return 1;
    }
    else{
        Elem *ant, *atual= *li;
        while(atual != NULL && ((atual->dados)->num_aluno < al.num_aluno)){
            ant = atual;
            atual = atual->prox;
        }
        if(atual == *li){
            no->prox = (*li);
            *li = no;
        }
        else{
            no->prox = ant->prox;
            ant->prox =no;
        }
        return 1;
    }

}
int consulta_lista_num(Lista *li,int num_aluno)
{
    int alt_opt;
    if(li==NULL)
        return 0;
    Elem *no=*li;
    while(no != NULL &&(no->dados->num_aluno!=num_aluno)){
        no = no->prox;
    }
    if(no==NULL)
        return 0;
    else{
        do{
            char *test = malloc(50*sizeof(char));
            printf("O que pretende alterar:\n1-Curso\n2-Ano de Matricula\n3-Regime\n4-Retroceder\n");
            scanf("%s",test);
            alt_opt = atoi(test);
                if(alt_opt==1){
                    printf("Introduza o novo curso:\n");
                    fflush(stdin);
                    fgets(no->dados->curso,50,stdin);
                    RetiraEnter(no->dados->curso);
                    converter(no->dados->curso);
                }
                else if(alt_opt==2){
                    printf("Introduza o ano de matricula:\n");
                    scanf("%d",&no->dados->ano_matricula);
                }
                else if(alt_opt==3){
                    printf("Introduza o regime:\n");
                    fflush(stdin);
                    fgets(no->dados->regime,50,stdin);
                    RetiraEnter(no->dados->regime);
                    converter(no->dados->regime);
                }
                else if(alt_opt==4){
                        break;

                }
            }while(alt_opt != 0);
        return 1;
    }
}

int consulta_lista_Apaga(Lista *li,int num_aluno)
{
    if(li==NULL)
        return 0;
    Elem *ant,*no=*li;
    while(no != NULL &&(no->dados->num_aluno!=num_aluno)){
        ant=no;
        no = no->prox;
    }
    if(no==NULL)
        return 0;
    if(no==*li)
        *li = no->prox;
    else
        ant->prox = no->prox;
    free(no);
    return 1;
}
int verifica_aluno(Lista *li,int num_aluno)
{
    if(li==NULL)
        return 0;
    Elem *no=*li;
    while(no != NULL &&(no->dados->num_aluno!=num_aluno)){
        no = no->prox;
    }
    if(no==NULL)
        return 0;
    else{
        printf("O aluno j� existe.\n");
        return 1;
    }



}

/*Fun��es Disciplinas*/

int ins_lis_ord_disci(Lista_D* li_D,struct disciplina dis){
    if(li_D == NULL){
        return 0;
    }
    Elem_D *no =(Elem_D*) malloc(sizeof(Elem));
    if(no==NULL){
        return 0;
    }
    (no->dados_D)= malloc(sizeof(struct disciplina));
    *(no->dados_D) = dis;
    if(lista_vazia_D(li_D)){
        no->prox_D = (*li_D);
        *li_D = no;
        return 1;
    }
    else{
        Elem_D *ant, *atual= *li_D;
        while(atual != NULL && atual->dados_D->nome[0] < dis.nome[0]){
            ant = atual;
            atual = atual->prox_D;
        }
        if(atual == *li_D){
            no->prox_D = (*li_D);
            *li_D = no;
        }
        else{
            no->prox_D = ant->prox_D;
            ant->prox_D =no;
        }
        return 1;
    }

}
int consulta_lista_dis(Lista_D *li_D,char *nome_disciplina)
{
    int alt_opt;
    if(li_D==NULL)
        return 0;
    Elem_D *no=*li_D;
    while(no != NULL && (StringsIguais (no->dados_D->nome,nome_disciplina))){
        no = no->prox_D;
    }
    if(no==NULL)
        return 0;
    else{
        do{
            char *test_D = malloc(50*sizeof(char));
            printf("O que pretende alterar:\n1-Nome da disciplina\n2-Docente\n3-Retroceder\n");
            scanf("%s",test_D);
            alt_opt = atoi(test_D);
                if(alt_opt==1){
                    printf("Introduza o novo nome:\n");
                    fflush(stdin);
                    fgets(no->dados_D->nome,50,stdin);
                    RetiraEnter(no->dados_D->nome);
                    converter(no->dados_D->nome);
                }
                else if(alt_opt==2){
                    printf("Introduza o novo docente:\n");
                    fflush(stdin);
                    fgets(no->dados_D->docente,50,stdin);
                    RetiraEnter(no->dados_D->docente);
                    converter(no->dados_D->docente);
                }
                else if(alt_opt==3){
                        break;
                }
            }while(alt_opt != 0);
        return 1;
    }
}

/*Fun�oes Exames*/

int ins_lis_ord_exames(Lista_E *li_E,Lista_D *li_D){

    char nome_disciplina[50];
    struct exame *endereco;
    struct exame exam;
    char sala_aux[5];
    int i=0;
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    consulta_lista_Endereco(li_D,nome_disciplina,endereco);
    exam.disciplina_exame=endereco->disciplina_exame;
    strcpy(exam.nome_disciplina,nome_disciplina);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    printf("Introduza a dura��o em minutos:\n");
    scanf("%d",&exam.duracao);
    printf("Introduza o numero de salas necess�rias:\n");
    scanf("%d",&exam.num_salas);
    printf("Introduza as salas:\n");
    while(i!=exam.num_salas)
    {
        fflush(stdin);
        fgets(sala_aux,5,stdin);
        RetiraEnter(sala_aux);
        converter(sala_aux);
        if(consulta_lista_Salas(li_E,sala_aux,exam.duracao,exam.data_exame)==1){
            printf("A sala encontra-se ocupada no hor�rio pretendido.\nIntroduza outra sala:\n");
        }
        else{
            strcpy((exam.sala[i]),sala_aux);
            i++;
            if(i!=exam.num_salas)
                printf("Pr�xima sala:\n");
            }
    }
    exam.num_alunos_inscr=0;
    if(li_E == NULL){
        return 0;
    }

    Elem_E *no =(Elem_E*)malloc(sizeof(Elem_E));
    if(no==NULL){
        return 0;
    }

    (no->dados_E)=malloc(sizeof(struct exame));
    *(no->dados_E) = exam;

    if(lista_vazia_E(li_E)){
        no->prox_E = (*li_E);
        *li_E = no;
        return 1;
    }
    else{
        Elem_E *ant, *atual= *li_E;
        while(atual != NULL && (atual->dados_E->data_exame.ano <= exam.data_exame.ano) &&
              (atual->dados_E->data_exame.mes <= exam.data_exame.mes)&&(atual->dados_E->data_exame.dia <= exam.data_exame.dia)){
            ant = atual;
            atual = atual->prox_E;
        }
        if(atual == *li_E){
            no->prox_E = (*li_E);
            *li_E = no;
        }
        else{
            no->prox_E = ant->prox_E;
            ant->prox_E =no;
        }
        return 1;
    }

}
int ins_lis_ord_exames_fich(Lista_E *li_E,struct exame exam){

    if(li_E == NULL){
        return 0;
    }

    Elem_E *no =(Elem_E*)malloc(sizeof(Elem_E));
    if(no==NULL){
        return 0;
    }

    (no->dados_E)=malloc(sizeof(struct exame));
    *(no->dados_E) = exam;

    if(lista_vazia_E(li_E)){
        no->prox_E = (*li_E);
        *li_E = no;
        return 1;
    }
    else{
        Elem_E *ant, *atual= *li_E;
        while(atual != NULL && (atual->dados_E->data_exame.ano <= exam.data_exame.ano) &&
              (atual->dados_E->data_exame.mes <= exam.data_exame.mes)&&(atual->dados_E->data_exame.dia <= exam.data_exame.dia)){
            ant = atual;
            atual = atual->prox_E;
        }
        if(atual == *li_E){
            no->prox_E = (*li_E);
            *li_E = no;
        }
        else{
            no->prox_E = ant->prox_E;
            ant->prox_E =no;
        }
        return 1;
    }

}

int consulta_lista_Endereco(Lista_D *li_D,char *nome_disciplina,struct exame *endereco)
{
    if(li_D==NULL)
        return 0;
    Elem_D *no=*li_D;
    while(no != NULL && (StringsIguais((no->dados_D->nome),nome_disciplina))){
        no = no->prox_D;
    }
    if(StringsIguais((no->dados_D->nome),nome_disciplina)!=0){
        return 0;
    }
    if(no==NULL)
        return 0;
    else{
        endereco->disciplina_exame=&(*(no->dados_D));
        return 1;
    }
}

int consulta_lista_Salas(Lista_E *li_E,char *sala_aux,int duracao,struct data data_aux)
{
    int i,j;
    int tamanho,inicio,fim,inicio_aux,fim_aux;
    if(li_E==NULL)
        return 0;
    Elem_E *no=*li_E;
    tamanho=conta_exam(li_E);
    for(i=0;i<tamanho;i++){
       for(j=0;j<29;j++){
        if(StringsIguais_E((no->dados_E->sala[j]),sala_aux)==1){
          if((no->dados_E->data_exame.ano)==data_aux.ano && (no->dados_E->data_exame.mes)==data_aux.mes && (no->dados_E->data_exame.dia)==data_aux.dia){
            inicio=(no->dados_E->data_exame.hora)*60 + no->dados_E->data_exame.minuto;
            fim=inicio + no->dados_E->duracao;
            inicio_aux=(data_aux.hora)*60+(data_aux.minuto);
            fim_aux=inicio_aux+duracao;
            if((inicio_aux<=fim && inicio_aux>=inicio) || (fim_aux<=fim && fim_aux>=inicio)){
                    return 1;
            }
          }
        }
       }
    no=no->prox_E;
    }
    if(no==NULL)
        return 0;
}
int consulta_lista_Apaga_Exam(Lista_E *li_E,Lista_D *li_D)
{
    char nome_disciplina[50];
    struct exame *endereco;
    struct exame exam;
    endereco = malloc(sizeof(struct exame));
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    consulta_lista_Endereco(li_D,nome_disciplina,endereco);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    if(li_E==NULL)
        return 0;
    Elem_E *ant,*no=*li_E;
    while(no != NULL)
    {
        if((StringsIguais_E(no->dados_E->disciplina_exame->nome,endereco->disciplina_exame->nome) && verifica_data(no->dados_E->data_exame,exam.data_exame)&& (StringsIguais_E(no->dados_E->epoca,exam.epoca)))==1)
        {
            break;
        }
        else{
        ant=no;
        no = no->prox_E;
        }
    }
    if(no==NULL)
        return 0;
    if(no==*li_E)
        *li_E = no->prox_E;
    else
        ant->prox_E = no->prox_E;
    free(no);
    return 1;
}

void print_list_E(Lista_E* li_E)
{
    int i;
    Elem_E *no = *li_E;
    while(no != NULL)
    {

        printf("Exame de: %s\n",no->dados_E->disciplina_exame->nome);
        printf("Epoca: %s\n",no->dados_E->epoca);
        printf("Duracao: %d\n",no->dados_E->duracao);
        printf("Horario: %d/%d/%d %d:%d\n",no->dados_E->data_exame.dia,no->dados_E->data_exame.mes,no->dados_E->data_exame.ano,no->dados_E->data_exame.hora,no->dados_E->data_exame.minuto);
        printf("Salas: ");
        for (i=0;i<no->dados_E->num_salas;i++){
                if(*(no->dados_E->sala[i])!=NULL)
                    printf("%s ",no->dados_E->sala[i]);
        }
        printf("\n");
        printf("Alunos inscritos: %d\n\n",no->dados_E->num_alunos_inscr);
        no = no->prox_E;

    }
}
int inscrever_alunos(Lista *li,Lista_E *li_E,Lista_D *li_D)
{
    char especial[9]={'E','S','P','E','C','I','A','L'};
    char normal[7]={'N','O','R','M','A','L'};
    char erasmus[8]={'E','R','A','S','M','U','S'};
    struct exame exam;
    int num_aluno;
    char nome_disciplina[50];
    struct exame *endereco;
    endereco= malloc(sizeof(struct exame));
    printf("Introduza o numero de aluno:\n");
    scanf("%d",&num_aluno);
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    consulta_lista_Endereco(li_D,nome_disciplina,endereco);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    if(li==NULL)
        return 0;
    Elem *no=*li;
    while(no != NULL &&(no->dados->num_aluno!=num_aluno)){
        no = no->prox;
    }
    if(no==NULL)
        return 0;
    else{
        Elem_E *no_E=*li_E;
        if(li_E==NULL){
            return 0;
        }
        while(no_E !=NULL){
        if((StringsIguais_E(no_E->dados_E->disciplina_exame->nome,endereco->disciplina_exame->nome) && verifica_data(no_E->dados_E->data_exame,exam.data_exame)&& (StringsIguais_E(no_E->dados_E->epoca,exam.epoca)))==1)
            break;
        else
            no_E = no_E->prox_E;
        }
        int i=0;
        for(;i<no->dados->numero_exames;i++){
        if((StringsIguais_E(no->dados->exame[i]->disciplina_exame->nome,endereco->disciplina_exame->nome)==1) &&(no->dados->exame[i]->data_exame.ano == exam.data_exame.ano)
            && (no->dados->exame[i]->data_exame.mes == exam.data_exame.mes) && (no->dados->exame[i]->data_exame.dia == exam.data_exame.dia)
            && (no->dados->exame[i]->data_exame.hora == exam.data_exame.hora) &&(no->dados->exame[i]->data_exame.minuto == exam.data_exame.minuto)){
                    printf("O aluno j� se encontra inscrito nesse exame.\n");
                    return 0;
            }
        }
        if (StringsIguais_E(no_E->dados_E->epoca,especial)==1){
            if(StringsIguais_E(no->dados->regime,normal)==1 ||  StringsIguais_E(no->dados->regime,erasmus)==1){
                printf("O aluno referido n�o pode ser inscrito neste exame.\n");
                return 0;
            }
            else{
                no->dados->exame[no->dados->numero_exames]=(no_E->dados_E);
                no_E->dados_E->alunos_inscritos[no_E->dados_E->num_alunos_inscr]=num_aluno;
                no->dados->numero_exames++;
                no_E->dados_E->num_alunos_inscr++;
                return 1;
            }

        }
        else{
            no->dados->exame[no->dados->numero_exames]=(no_E->dados_E);
            no_E->dados_E->alunos_inscritos[no_E->dados_E->num_alunos_inscr]=num_aluno;
            no->dados->numero_exames++;
            no_E->dados_E->num_alunos_inscr++;
            return 1;
        }


        }

}

int desinscrever_alunos(Lista *li,Lista_D *li_D,Lista_E *li_E)
{
    struct exame exam;
    int num_aluno;
    char nome_disciplina[50];
    struct exame *endereco;
    endereco= malloc(sizeof(struct exame));
    printf("Introduza o numero de aluno:\n");
    scanf("%d",&num_aluno);
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    consulta_lista_Endereco(li_D,nome_disciplina,endereco);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    if(li==NULL)
        return 0;
    Elem *no=*li;
    while(no != NULL &&(no->dados->num_aluno!=num_aluno)){
        no = no->prox;
    }
    if(no==NULL)
        return 0;
    else{
        Elem_E *no_E=*li_E;
        if(li_E==NULL){
            return 0;
        }
        while(no_E !=NULL){
        if((StringsIguais_E(no_E->dados_E->disciplina_exame->nome,endereco->disciplina_exame->nome) && verifica_data(no_E->dados_E->data_exame,exam.data_exame)&& (StringsIguais_E(no_E->dados_E->epoca,exam.epoca)))==1)
            break;
        else
            no_E = no_E->prox_E;
        }

        int i=0;
        for(;i<no->dados->numero_exames;i++){
        if((StringsIguais_E(no->dados->exame[i]->disciplina_exame->nome,endereco->disciplina_exame->nome)==1) &&(no->dados->exame[i]->data_exame.ano == exam.data_exame.ano)
            && (no->dados->exame[i]->data_exame.mes == exam.data_exame.mes) && (no->dados->exame[i]->data_exame.dia == exam.data_exame.dia)
            && (no->dados->exame[i]->data_exame.hora == exam.data_exame.hora) &&(no->dados->exame[i]->data_exame.minuto == exam.data_exame.minuto)){
                   no->dados->exame[i]=NULL;
                   no->dados->numero_exames--;
                   no_E->dados_E->num_alunos_inscr--;
                   return 1;

            }
        }

    }
}
int configura_salas(Lista_E *li_E)
{
        char nome_disciplina[50];
        char sala_aux[5];
        struct data data;
        int i,tamanho,j;
        char sala[5];
        int alt_opt;
        printf("Introduza o nome da disciplina:\n");
        fflush(stdin);
        fgets(nome_disciplina,50,stdin);
        RetiraEnter(nome_disciplina);
        converter(nome_disciplina);
        printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
        scanf("%d/%d/%d/%d/%d",&data.dia,&data.mes,&data.ano,&data.hora,&data.minuto);
        if(li_E==NULL)
            return 0;
        Elem_E *no_E=*li_E;
        tamanho=conta_exam(li_E);
        while(no_E != NULL)
        {
            if((StringsIguais_E(no_E->dados_E->disciplina_exame->nome,nome_disciplina) && verifica_data(no_E->dados_E->data_exame,data))==1)
                break;
            else
                no_E = no_E->prox_E;
        }
        do{
            char *test_D = malloc(50*sizeof(char));
            printf("O que pretende fazer:\n1-Alterar salas\n2-Apagar salas\n3-Introduzir nova sala\n4-Retroceder\n");
            scanf("%s",test_D);
            alt_opt = atoi(test_D);
                if(alt_opt==1){
                    printf("Introduza a sala que pretende alterar:\n");
                    fflush(stdin);
                    fgets(sala,5,stdin);
                    RetiraEnter(sala);
                    converter(sala);
                    for(i=0;i<tamanho;i++){
                        for(j=0;j<29;j++){
                            if(StringsIguais_E((no_E->dados_E->sala[j]),sala)==1){
                                    printf("Introduza a nova sala:\n");
                                    fflush(stdin);
                                    fgets(sala_aux,5,stdin);
                                    RetiraEnter(sala_aux);
                                    converter(sala_aux);
                                    if(consulta_lista_Salas(li_E,sala_aux,no_E->dados_E->duracao,data)==1){
                                        printf("A sala encontra-se ocupada no hor�rio pretendido.\n");
                                    }
                                    else
                                        strcpy(no_E->dados_E->sala[j],sala_aux);
                            }
                        }
                    }
                }
                else if(alt_opt==2){
                    printf("Introduza a sala que pretende apagar:\n");
                    fflush(stdin);
                    fgets(sala,5,stdin);
                    RetiraEnter(sala);
                    converter(sala);
                    for(i=0;i<tamanho;i++){
                        for(j=0;j<29;j++){
                            if(StringsIguais_E(sala,no_E->dados_E->sala[j])==1){
                                    *(no_E->dados_E->sala[j])=NULL;
                                    no_E->dados_E->num_salas--;

                            }
                        }
                    }
                }

                else if(alt_opt==3){
                    printf("Introduza a nova sala:\n");
                    fflush(stdin);
                    fgets(sala_aux,5,stdin);
                    RetiraEnter(sala_aux);
                    converter(sala_aux);
                    if(consulta_lista_Salas(li_E,sala_aux,no_E->dados_E->duracao,data)==1){
                        printf("A sala encontra-se ocupada no hor�rio pretendido.\n");
                    }
                    else{
                        strcpy((no_E->dados_E->sala[no_E->dados_E->num_salas]),sala_aux);
                        no_E->dados_E->num_salas++;
                    }
                    }
                else if(alt_opt==4)
                    break;
            }while(alt_opt != 0);
}

int listar_alunos(Lista *li)
{
    struct exame exam;
    int num_aluno;
    char nome_disciplina[50];
    int i;
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    if(li==NULL)
        return 0;
    Elem *no=*li;
    printf("Alunos inscritos: ");
    while(no != NULL)
    {
        for(i=0;i<no->dados->numero_exames;i++){
            if(((StringsIguais_E(no->dados->exame[i]->disciplina_exame->nome,nome_disciplina)) && verifica_data(no->dados->exame[i]->data_exame,exam.data_exame) && (StringsIguais_E(no->dados->exame[i]->epoca,exam.epoca)))==1)
                printf("%d ",no->dados->num_aluno);
        }
        no = no->prox;
    printf("\n");
    if(no==NULL)
        return 0;
    }
    return 1;
}
int verifica_salas(Lista_E *li_E)
{
    struct exame exam;
    char nome_disciplina[50];
    int i;
    printf("Introduza o nome da disciplina:\n");
    fflush(stdin);
    fgets(nome_disciplina,50,stdin);
    RetiraEnter(nome_disciplina);
    converter(nome_disciplina);
    printf("Introduza a epoca do exame:(normal , recurso ou especial)\n");
    fflush(stdin);
    fgets(exam.epoca,50,stdin);
    RetiraEnter(exam.epoca);
    converter(exam.epoca);
    printf("Introduza a data(dia/mes/ano/hora/minuto):\n");
    scanf("%d/%d/%d/%d/%d",&exam.data_exame.dia,&exam.data_exame.mes,&exam.data_exame.ano,&exam.data_exame.hora,&exam.data_exame.minuto);
    Elem_E *no_E=*li_E;
    if(li_E==NULL){
        return 0;
    }
    while(no_E !=NULL){
        if((StringsIguais_E(no_E->dados_E->disciplina_exame->nome,nome_disciplina) && verifica_data(no_E->dados_E->data_exame,exam.data_exame)&& (StringsIguais_E(no_E->dados_E->epoca,exam.epoca)))==1)
            break;
        else
            no_E = no_E->prox_E;
    }
    printf("Salas reservadas: ");
    for (i=0;i<no_E->dados_E->num_salas;i++){
            if(*(no_E->dados_E->sala[i])!=NULL)
                printf("%s ",no_E->dados_E->sala[i]);
    }
    printf("\n");
    double conta;
    conta=((no_E->dados_E->num_alunos_inscr)-(no_E->dados_E->num_salas)*30)/30.0;
    if(no_E->dados_E->num_salas*30 < no_E->dados_E->num_alunos_inscr){
        printf("N�o possui salas suficientes.\n");
        printf("S�o precisas %.0f salas.",ceil(conta));
        return 0;
    }
    return 1;

}

/*Ler e Escrever nos ficheiros*/

int escrever_ficheiro(Lista *li){
    FILE *fich;
    FILE *fich_num;
    fich = fopen("alunos.txt","wb");
    fich_num = fopen("num_alunos.txt","wb");
    if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int i=0;
    int num;
    num=conta_exam_aluno(li);
    if(li==NULL)
        return 0;
    Elem *no=*li;
    while(no != NULL){
        fseek(fich,i*sizeof(struct aluno),SEEK_SET);
        fwrite((no->dados),sizeof(struct aluno),1,fich);
        i++;
        no = no->prox;
    }
    fwrite(&num,sizeof(int),1,fich_num);
    fclose(fich);
    fclose(fich_num);
    if(no==NULL)
        return 0;
}

int ler_ficheiros(Lista* li){
    FILE *fich;
    FILE *fich_num;
    fich=fopen("alunos.txt","rb");
     if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    fich_num=fopen("num_alunos.txt","rb");
     if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int num;
    fread(&num,sizeof(int),1,fich_num);
    int i=0;
    struct aluno texto;
    for(;i<num;i++){
    fseek(fich,i*sizeof(struct aluno),SEEK_SET);
    fread(&(texto),sizeof(struct aluno),1,fich);
    ins_lis_ord_alunos(li,texto);
    }
    fclose(fich);
    fclose(fich_num);


}

int escrever_ficheiro_disc(Lista_D *li_D){
    FILE *fich;
    FILE *fich_num;
    fich = fopen("disciplinas.txt","wb");
    fich_num = fopen("num_disciplinas.txt","wb");
    if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int i=0;
    int num;
    num=conta_disc(li_D);
    if(li_D==NULL)
        return 0;
    Elem_D *no=*li_D;
    while(no != NULL){
        fseek(fich,i*sizeof(struct disciplina),SEEK_SET);
        fwrite((no->dados_D),sizeof(struct disciplina),1,fich);
        i++;
        no = no->prox_D;
    }
    fwrite(&num,sizeof(int),1,fich_num);
    fclose(fich);
    fclose(fich_num);
    if(no==NULL)
        return 0;
}

int ler_ficheiros_disc(Lista_D* li_D){
    FILE *fich;
    FILE *fich_num;
    fich=fopen("disciplinas.txt","rb");
     if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    fich_num=fopen("num_disciplinas.txt","rb");
     if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int num;
    fread(&num,sizeof(int),1,fich_num);
    int i=0;
    struct disciplina texto;
    for(;i<num;i++){
    fseek(fich,i*sizeof(struct disciplina),SEEK_SET);
    fread(&(texto),sizeof(struct disciplina),1,fich);
    ins_lis_ord_disci(li_D,texto);
    }
    fclose(fich);
    fclose(fich_num);
}

int escrever_ficheiro_exame(Lista_E *li_E){
    FILE *fich;
    FILE *fich_num;
    fich = fopen("exames.txt","wb");
    fich_num = fopen("num_exames.txt","wb");
    if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int i=0;
    int num;
    num=conta_exam(li_E);
    if(li_E==NULL)
        return 0;
    Elem_E *no=*li_E;
    while(no != NULL){
        fseek(fich,i*sizeof(struct exame),SEEK_SET);
        fwrite((no->dados_E),sizeof(struct exame),1,fich);
        i++;
        no = no->prox_E;
    }
    fwrite(&num,sizeof(int),1,fich_num);
    fclose(fich);
    fclose(fich_num);
    if(no==NULL)
        return 0;
}

int ler_ficheiros_exame(Lista_E* li_E){
    FILE *fich;
    FILE *fich_num;
    fich=fopen("exames.txt","rb");
     if(fich == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    fich_num=fopen("num_exames.txt","rb");
     if(fich_num == NULL){
        printf("Erro.\n");
        system("pause");
        exit(1);
    }
    int num;
    fread(&num,sizeof(int),1,fich_num);
    int i=0;
    struct exame texto;
    for(;i<num;i++){
    fseek(fich,i*sizeof(struct exame),SEEK_SET);
    fread(&(texto),sizeof(struct exame),1,fich);
    ins_lis_ord_exames_fich(li_E,texto);
    }
    fclose(fich);
    fclose(fich_num);
}

int StringsIguais_E(char *s1, char *s2)
{

    while(*s1==*s2 && *s1!= '\0'&& *s2!='\0'){
        s2++;
        s1++;
    }
    if(*s1=='\0'&& *s2 == '\0'){
        return 1;
    }
    else{
        return 0;
    }
}
int StringsIguais(char s1[], char *s2)
{
    int i=0;
    while(s1[i]==*s2 && s1[i] != '\0'&& *s2!='\0'){
        s2++;
        i++;
    }
    if(s1[i]=='\0'&& *s2 == '\0'){
        return 0;
    }
    else{
        return 1;
    }
}
void RetiraEnter(char *str){
    int i=0;
    while(str[i]!= '\n')
        i++;
    str[i]='\0';

}
int verifica_data(struct data data1,struct data data2)
{
    if((data1.dia==data2.dia) && (data1.mes==data2.mes) && (data1.ano==data2.ano) && (data1.hora==data2.hora) && (data1.minuto==data2.minuto)){
        return 1;
    }
    else{
        return 0;
    }
}

int conta_exam(Lista_E* li_E)
{   int conta=0;
    Elem_E *no = *li_E;
    while(no != NULL)
    {
        conta++;
        no = no->prox_E;

    }
    return conta;
}

int conta_exam_aluno(Lista* li)
{   int conta=0;
    Elem *no = *li;
    while(no != NULL)
    {
        conta++;
        no = no->prox;

    }
    return conta;
}

int conta_disc(Lista_D* li_D)
{   int conta=0;
    Elem_D *no = *li_D;
    while(no != NULL)
    {
        conta++;
        no = no->prox_D;
    }
    return conta;
}

int consulta_novo_endereco(Lista_D *li_D,Lista_E *li_E)
{
    struct exame *endereco;
    if(li_D==NULL)
        return 0;
    Elem_D *no=*li_D;
    while(no != NULL){
        Elem_E *no_E=*li_E;
        if(li_E==NULL)
            return 0;
        while(no_E !=NULL)
        {
        if((StringsIguais_E(no_E->dados_E->nome_disciplina,no->dados_D->nome))==1){
            consulta_lista_Endereco(li_D,no_E->dados_E->nome_disciplina,endereco);
            no_E->dados_E->disciplina_exame=endereco->disciplina_exame;
        }
        no_E = no_E->prox_E;
        }
        no = no->prox_D;
        }
    return 0;
}

int consulta_novo_endereco_aluno(Lista_E *li_E,Lista *li)
{
    struct exame *endereco;
    int i,j;
    if(li_E==NULL)
        return 0;
    Elem_E *no_E=*li_E;
    while(no_E != NULL){
        Elem *no=*li;
        if(li==NULL)
            return 0;
        while(no!=NULL)
        {
        for(i=0;i<=no->dados->numero_exames;i++){
                for(j=0;j<=no_E->dados_E->num_alunos_inscr;j++)
                    if(no->dados->num_aluno==no_E->dados_E->alunos_inscritos[j]) {
                        no->dados->exame[i]=(no_E->dados_E);
            }
        }
        no = no->prox;
        }
        no_E = no_E->prox_E;
        }
return 0;
}


int converter(char *s1){
    int i=0;
    while(s1[i]!='\0'){
        s1[i]=toupper(s1[i]);
        i++;
    }
    return 0;
}



